from setuptools import setup

setup(
    name='json2csv_parser',
    version='0.0.1',
    packages=[''],
    url='',
    license='',
    author='adamkeboola',
    author_email='adam.bako@keboola.com',
    description='Tool for parsing JSON into CSV files'
)
